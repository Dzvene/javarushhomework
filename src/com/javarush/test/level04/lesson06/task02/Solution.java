package com.javarush.test.level04.lesson06.task02;

/* Максимум четырех чисел
Ввести с клавиатуры четыре числа, и вывести максимальное из них.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());
        int c = Integer.parseInt(reader.readLine());
        int d = Integer.parseInt(reader.readLine());

        int y;
        int z;

        if(a > b){
            y = a;
        }else{
            y = b;
        }

        if(c > d){
            z = c;
        }else{
            z = d;
        }

        if(z > y){
            System.out.println(z);
        }else{
            System.out.println(y);
        }

    }
}
