package com.javarush.test.level12.lesson04.task03;

/* Пять методов print с разными параметрами
Написать пять методов print с разными параметрами.
*/

public class Solution {
	public static void main(String[] args) {

	}

	//Напишите тут ваши методы
	public int print(int i){
		return i;
	}

	public Integer print(Integer i){
		return i;
	}

	public String print(String i){
		return i;
	}

	public String print(String a, int b){
		return a + b;
	}

	public String print(String a, int b, int c){
		return a + b;
	}
}
