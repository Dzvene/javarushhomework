package com.javarush.test.level12.lesson04.task05;

/* Три метода возвращают максимальное из двух переданных в него чисел
Написать public static методы: int max(int, int), long max (long, long), double max (double, double).
Каждый метод должен возвращать максимальное из двух переданных в него чисел.
*/

public class Solution {
	public static void main(String[] args) {

	}

	//Напишите тут ваши методы

	public int max(int a, int b){
		int temp;

		if(a > b){
			temp = a;
		}else{
			temp = b;
		}
		return temp;
	}

	public long max(long a, long b){
		long temp;

		if(a > b){
			temp = a;
		}else{
			temp = b;
		}
		return temp;
	}

	public double max(double a, double b) {
		double temp;

		if(a > b){
			temp = a;
		}else{
			temp = b;
		}
		return temp;
	}
}
