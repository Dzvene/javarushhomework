package com.javarush.test.level14.lesson08.bonus03;

public class Singleton {

	private static Singleton INSTANCE = null;

	private Singleton() {

	}

	static Singleton getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Singleton();
		}
		return INSTANCE;
	}
}
