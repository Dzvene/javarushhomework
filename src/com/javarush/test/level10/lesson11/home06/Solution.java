package com.javarush.test.level10.lesson11.home06;

/* Конструкторы класса Human
Напиши класс Human с 6 полями. Придумай и реализуй 10 различных конструкторов для него. Каждый конструктор должен иметь смысл.
*/

public class Solution {
	public static void main(String[] args) {
		

	}

	public static class Human {
		//напишите тут ваши переменные и конструкторы
		String name;
		int age;
		int height;
		int weight;
		String sex;
		String marrited;
		String child;
		String parents;
		String aducation;
		String job;


		public Human(String name) {
			this.name = name;
		}

		public Human(String name, int age, int height) {
			this.name = name;
			this.age = age;
			this.height = height;
		}

		public Human(String name, int age, String aducation) {
			this.name = name;
			this.age = age;
			this.aducation = aducation;
		}

		public Human(String marrited, String child, String parents) {
			this.marrited = marrited;
			this.child = child;
			this.parents = parents;
		}

		public Human(String name, int height, String child, String aducation) {
			this.name = name;
			this.height = height;
			this.child = child;
			this.aducation = aducation;
		}

		public Human(String name, int age, int height, int weight, String sex, String marrited, String child, String parents, String aducation, String job) {
			this.name = name;
			this.age = age;
			this.height = height;
			this.weight = weight;
			this.sex = sex;
			this.marrited = marrited;
			this.child = child;
			this.parents = parents;
			this.aducation = aducation;
			this.job = job;
		}

		public Human(String name, int age, int height, int weight) {
			this.name = name;
			this.age = age;
			this.height = height;
			this.weight = weight;
		}

		public Human(String name, int age, int height, String sex, String parents, String job) {
			this.name = name;
			this.age = age;
			this.height = height;
			this.sex = sex;
			this.parents = parents;
			this.job = job;
		}

		public Human(String name, int age, int height, int weight, String sex, String marrited, String child, String parents) {
			this.name = name;
			this.age = age;
			this.height = height;
			this.weight = weight;
			this.sex = sex;
			this.marrited = marrited;
			this.child = child;
			this.parents = parents;
		}

		public Human(String name, int age, int height, int weight, String sex, String marrited, String child, String parents, String aducation) {
			this.name = name;
			this.age = age;
			this.height = height;
			this.weight = weight;
			this.sex = sex;
			this.marrited = marrited;
			this.child = child;
			this.parents = parents;
			this.aducation = aducation;
		}
	}
}
