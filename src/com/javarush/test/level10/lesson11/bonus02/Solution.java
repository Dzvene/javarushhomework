package com.javarush.test.level10.lesson11.bonus02;


/* Нужно добавить в программу новую функциональность
Задача: Программа вводит с клавиатуры пару (число и строку) и выводит их на экран.
Новая задача: Программа вводит с клавиатуры пары (число и строку), сохраняет их в HashMap.
Пустая строка – конец ввода данных. Числа могу повторяться. Строки всегда уникальны. Введенные данные не должны потеряться!
Затем программа выводит содержание HashMap на экран.

Пример ввода:
1
Мама
2
Рама
1
Мыла

Пример вывода:
1 Мыла
2 Рама
1 Мама
*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class Solution {

	public static void main(String[] args) throws IOException {

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		HashMap<String, Integer> map = new HashMap<>();

		while (true){

			String number = reader.readLine();

			if (number.isEmpty()){
				break;
			}else{
				String name = reader.readLine();

				if (name.isEmpty()){
					break;
				}else{
					map.put(name, Integer.parseInt(number));
				}
			}
		}



		for (HashMap.Entry<String, Integer> m : map.entrySet() ){
			System.out.println(m.getValue() + " " + m.getKey());
		}

	}
}
