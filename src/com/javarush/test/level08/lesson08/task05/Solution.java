package com.javarush.test.level08.lesson08.task05;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* Удалить людей, имеющих одинаковые имена
Создать словарь (Map<String, String>) занести в него десять записей по принципу «фамилия» - «имя».
Удалить людей, имеющих одинаковые имена.
*/

public class Solution
{
	public static HashMap<String, String> createMap()
	{
		HashMap<String, String> map = new HashMap<>();

		map.put("Крячко", "Сергей");
		map.put("Шумейко", "Ольга");
		map.put("Ендовицкий", "Максим");
		map.put("Дякун", "Людмила");
		map.put("Дякун", "Александр");
		map.put("Дякун", "Ольга");
		map.put("Табаков", "Александр");
		map.put("Ярмантович", "Станислав");
		map.put("Скочеляс", "Ольга");
		map.put("Хомышин", "Антон");//Напишите тут ваш код

		return map;
	}

	public static void removeTheFirstNameDuplicates(HashMap<String, String> map)
	{
		//Копируем переданную мапу 2 раза
		HashMap<String, String> map2 = new HashMap<String, String>(map);
		HashMap<String, String> map3 = new HashMap<String, String>(map);

		//цикл по map2
		for (Map.Entry<String, String> pair : map2.entrySet())
		{
			map3.remove(pair.getKey());
			if (map3.containsValue(pair.getValue())){
				removeItemFromMapByValue(map, pair.getValue());
			}
		}

	}

	public static void removeItemFromMapByValue(HashMap<String, String> map, String value)
	{
		HashMap<String, String> copy = new HashMap<String, String>(map);
		for (Map.Entry<String, String> pair : copy.entrySet()) {
			if (pair.getValue().equals(value))
				map.remove(pair.getKey());
		}
	}


}