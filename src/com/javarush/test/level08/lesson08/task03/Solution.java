package com.javarush.test.level08.lesson08.task03;

import java.util.HashMap;
import java.util.Map;

/* Одинаковые имя и фамилия
Создать словарь (Map<String, String>) занести в него десять записей по принципу «Фамилия» - «Имя».
Проверить сколько людей имеют совпадающие с заданным имя или фамилию.
*/

public class Solution {
	static int a;
	static int b;

	public static HashMap<String, String> createMap() {
		//Напишите тут ваш код
		HashMap<String, String> map = new HashMap<String, String>();

		map.put("Surname1", "Name1");
		map.put("Surname2", "Name2");
		map.put("Surname3", "Name1");
		map.put("Surname4", "Name1");
		map.put("Surname5", "Name3");
		map.put("Surname6", "Name1");
		map.put("Surname7", "Name1");
		map.put("Surname8", "Name1");
		map.put("Surname9", "Name5");
		map.put("Surname10", "Name1");

		return map;
	}

	public static int getCountTheSameFirstName(HashMap<String, String> map, String FirstName) {
		//Напишите тут ваш код
		int count = 0;
		for (Map.Entry<String, String> pair : map.entrySet()) {
			String value = pair.getValue();
			if (FirstName.equals(value)) {
				count++;
			}
		}
		return count;
	}

	public static int getCountTheSameLastName(HashMap<String, String> map, String LastName) {
		//Напишите тут ваш код
		int count = 0;
		for (Map.Entry<String, String> pair : map.entrySet()) {
			String key = pair.getKey();
			if (LastName.equals(key)) {
				count++;
			}
		}
		return count;
	}

}
