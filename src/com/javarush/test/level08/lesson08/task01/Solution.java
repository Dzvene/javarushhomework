package com.javarush.test.level08.lesson08.task01;

import java.util.HashSet;
import java.util.Set;

/* 20 слов на букву «Л»
Создать множество строк (Set<String>), занести в него 20 слов на букву «Л».
*/

public class Solution
{
    public static HashSet<String> createSet()
    {
        //напишите тут ваш код
        HashSet<String> set = new HashSet<>();


        set.add("Лист1");
        set.add("Лист2");
        set.add("Лист3");
        set.add("Лист4");
        set.add("Лист5");
        set.add("Лист6");
        set.add("Лист7");
        set.add("Лист8");
        set.add("Лист9");
        set.add("Лист10");
        set.add("Лист11");
        set.add("Лист12");
        set.add("Лист13");
        set.add("Лист14");
        set.add("Лист15");
        set.add("Лист16");
        set.add("Лист17");
        set.add("Лист18");
        set.add("Лист19");
        set.add("Лист20");

        return set;

    }
}
