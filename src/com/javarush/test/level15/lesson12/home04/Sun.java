package com.javarush.test.level15.lesson12.home04;

public class Sun implements Planet{

	private static Sun INSTANCE= null;

	private Sun() {}

	public static synchronized Sun getInstance() {
		if (INSTANCE == null)
			INSTANCE = new Sun();
		return INSTANCE;
	}
}
