package com.javarush.test.level15.lesson12.home04;

public class Earth implements Planet{

	private static Earth INSTANCE= null;

	private Earth() {}

	public static synchronized Earth getInstance() {
		if (INSTANCE == null)
			INSTANCE = new Earth();
		return INSTANCE;
	}
}
