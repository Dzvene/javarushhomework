package com.javarush.test.level15.lesson12.home04;

public class Moon implements Planet{

	private static Moon INSTANCE= null;

	private Moon() {}

	public static synchronized Moon getInstance() {
		if (INSTANCE == null)
			INSTANCE = new Moon();
		return INSTANCE;
	}
}
