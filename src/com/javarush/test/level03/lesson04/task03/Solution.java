package com.javarush.test.level03.lesson04.task03;

/* StarCraft
Создать 10 зергов, 5 протосов и 12 терран.
Дать им всем уникальные имена.
*/

public class Solution
{
    public static void main(String[] args)
    {
        //напишите тут ваш код
        Zerg zerg1 = new Zerg();
        Zerg zerg2 = new Zerg();
        Zerg zerg3 = new Zerg();
        Zerg zerg4 = new Zerg();
        Zerg zerg5 = new Zerg();
        Zerg zerg6 = new Zerg();
        Zerg zerg7 = new Zerg();
        Zerg zerg8 = new Zerg();
        Zerg zerg9 = new Zerg();
        Zerg zerg10 = new Zerg();

        zerg1.name = "z01";
        zerg2.name = "z02";
        zerg3.name = "z03";
        zerg4.name = "z04";
        zerg5.name = "z05";
        zerg6.name = "z06";
        zerg7.name = "z07";
        zerg8.name = "z08";
        zerg9.name = "z09";
        zerg10.name = "z10";

        Protos protos1 = new Protos();
        Protos protos2 = new Protos();
        Protos protos3 = new Protos();
        Protos protos4 = new Protos();
        Protos protos5 = new Protos();

        protos1.name = "p01";
        protos2.name = "p02";
        protos3.name = "p03";
        protos4.name = "p04";
        protos5.name = "p05";


        Terran terran1 = new Terran();
        Terran terran2 = new Terran();
        Terran terran3 = new Terran();
        Terran terran4 = new Terran();
        Terran terran5 = new Terran();
        Terran terran6 = new Terran();
        Terran terran7 = new Terran();
        Terran terran8 = new Terran();
        Terran terran9 = new Terran();
        Terran terran10 = new Terran();
        Terran terran11 = new Terran();
        Terran terran12 = new Terran();

        terran1.name = "t01";
        terran2.name = "t02";
        terran3.name = "t03";
        terran4.name = "t04";
        terran5.name = "t05";
        terran6.name = "t06";
        terran7.name = "t07";
        terran8.name = "t08";
        terran9.name = "t09";
        terran10.name = "t10";
        terran11.name = "t11";
        terran12.name = "t12";
    }

    public static class Zerg
    {
        public String name;
    }

    public static class Protos
    {
        public String name;
    }

    public static class Terran
    {
        public String name;
    }
}