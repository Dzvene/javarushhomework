package com.javarush.test.level05.lesson12.bonus02;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* Нужно добавить в программу новую функциональность
Задача: Программа вводит два числа с клавиатуры и выводит минимальное из них на экран.
Новая задача: Программа вводит пять чисел с клавиатуры и выводит минимальное из них на экран.
*/

public class Solution {

	public static void main(String[] args) throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		int a = Integer.parseInt(reader.readLine());
		int b = Integer.parseInt(reader.readLine());
		int c = Integer.parseInt(reader.readLine());
		int d = Integer.parseInt(reader.readLine());
		int e = Integer.parseInt(reader.readLine());

		int minimum = min(a, b, c, d, e);

		System.out.println("Minimum = " + minimum);
	}


	public static int min(int a, int b, int c, int d, int e) {

		int a1;
		int a2;
		int a3;
		int a4 = 0;

		if (a < b){
			a1 = a;
		}else{
			a1 = b;
		}

		if (c < d){
			a2 = c;
		}else{
			a2 = d;
		}

		if (a1 < a2){
			a3 = a1;
		}else{
			a3 = a2;
		}

		if (a3 < c){
			a4 = a3;
		}else{
			a4 = c;
		}

		return a4;
	}

}





















