package com.javarush.test.level09.lesson11.home09;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* Десять котовФ
Создать класс кот – Cat, с полем «имя» (String).
Создать словарь Map(<String, Cat>) и добавить туда 10 котов в виде «Имя»-«Кот».
Получить из Map множество(Set) всех имен и вывести его на экран.
*/

public class Solution {

	public static void main(String[] args) {

		Map<String, Cat> map = createMap();
		Set<Cat> set = convertMapToSet(map);
		printCatSet(set);
	}

	public static Map<String, Cat> createMap() {
		//напишите тут ваш код

		Map<String, Cat> cat = new HashMap<>();

		cat.put("cat1", new Cat("cat1"));
		cat.put("cat2", new Cat("cat2"));
		cat.put("cat3", new Cat("cat3"));
		cat.put("cat4", new Cat("cat4"));
		cat.put("cat5", new Cat("cat5"));
		cat.put("cat6", new Cat("cat6"));
		cat.put("cat7", new Cat("cat7"));
		cat.put("cat8", new Cat("cat8"));
		cat.put("cat9", new Cat("cat9"));
		cat.put("cat10", new Cat("cat10"));

		return cat;
	}

	public static Set<Cat> convertMapToSet(Map<String, Cat> map) {
		//напишите тут ваш код

		Set<Cat> set = new HashSet<>();

		for (Map.Entry<String, Cat> m : map.entrySet()){
			set.add(m.getValue());
		}

		return set;
	}

	public static void printCatSet(Set<Cat> set) {
		for (Cat cat : set) {
			System.out.println(cat);
		}
	}

	public static class Cat {
		private String name;

		public Cat(String name) {
			this.name = name;
		}

		public String toString() {
			return "Cat " + this.name;
		}
	}


}
